'''
    in this example, write the parsed twitter information to mysql database
    
    NOTE: before run this, create the necessary database tables, by executing twitter_ddl.sql using phpMyadmin    
    http://msba6320.oit.umn.edu:122/myadmin/   
    
        
    #1-6: test it without writting to db
    #7: test it without db writting
    #8-10: test it with live connection + db write.
    
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils

#1. below we import an enhanced version of SimpleMysql
# from pylib.simplemysql import SimpleMysql

_debug_ = True #implement debug model behavior
_online_ = False #read from online sources
#8. change _online_ to True, test live connection db write.

#2. add _writedb_ global variable.
# _writedb_ = False #whether write outputs to database.

#7. change _writedb_ to true, test db writting.

utils.enable_utf_print() 

#these fields can be directly copied
tweet_fields = {
    'id':'tweet_id',
    'text':'tweet_text',
    'source':'source',
    'retweet_count':'retweet_count',    
    'in_reply_to_status_id':'reply_tweet_id',
    'in_reply_to_user_id':'reply_user_id',
    'favorite_count':'favorite_count',
    'created_at':'created_at',
}

#to users table
user_fields = {
    'name':'name',
    'screen_name':'screen_name',
    'id':'user_id',
}

def get_fields(source, fields):
    '''
        given a source and a destination dict, copy the fields from source to destination according to the mapping provided by fields.  e.g. fields['fielda']='fieldb' would map fielda in source to fieldb in dest.     
    '''
    dest={}    
    if not isinstance(source, dict):
        print "source is not a dict" + type(source)

    for field in fields:
        if source.get(field):
            dest[fields[field]]=source[field]
    return dest 

class CustomStreamListener(tweepy.StreamListener):

    def on_data(self, raw_tweet):
        #data comes in the raw json string
        try:    
            capture_date = utils.mysql_time()
            
            tweet = json.loads(raw_tweet)
            if tweet.get('delete'):
                return True ## skip delete tweet
                
            tweet_data = {}
            user_data = {}
            
            # retrieve and store tweet_data    
            # the function utils.map_fields does the same thing as get_fields()
            tweet_data.update(get_fields(tweet,tweet_fields))
            
            if not tweet_data.get('tweet_id'):
                if _debug_:
                    #save the tweet for debugging
                    utils.dump_json(tweet,'debug/tweet.txt')
                print('No tweet id')
                return True  #discard this tweet 
            
            #fields that need special processing
            if tweet_data.get('created_at'):
                tweet_data['created_at']=utils.utc_to_mysql_time(tweet_data['created_at'])
                    
            if tweet.get('user'):
                # retrieve most of fields.
                user_data.update(get_fields(tweet['user'],user_fields))    
                if not user_data.get('user_id'):
                    if _debug_:
                        utils.dump_json(tweet,'debug/tweet%s.txt'%tweet_data.get('tweet_id'))
                    print('No tweet user id')
                    return True  #discard this tweet         

            #enrichment    
            if tweet_data: 
                tweet_data['user_id'] = user_data.get('user_id')
                tweet_data['capture_date']=capture_date
            if user_data:
                user_data['capture_date']=capture_date
                #period is used as one of the keys so that
                #we do not insert the same users twice
                #in the one day
                user_data['period'] = capture_date[:10]        
            
            #3. print out a message like: "2014-01-01 12:23:12 tweet id: 3239472937"
            # print "%s tweet id:%s"%(capture_date,tweet_data.get('tweet_id'))
            
            #4. if _writedb_ is True, then use SimpleMysql's insert
            # -- to insert tweet_data and user_data into tweets and users table 
            # -- respectively, and print out how many rows have been inserted.            
            # -- e.g., tweets 1 inserted.
            # -- if  _writedb_ is false, simply print the data.
            
            # for table, data in (('users',user_data),('tweets',tweet_data)):
                # if data: 
                    # if _writedb_:
                        # print "%i %s inserted"%(db.insert(table, data), table)
                    # else:
                        # print table
                        # print data
                        # print   
            # print             
        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_json(tweet,'debug/tweet.txt')
                raise 
            else:
                pass #ignore errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()
    
    #5. if _writedb_ is True, connect to db
    # if _writedb_:
        # db = SimpleMysql(
            # host=config.db_host,
            # db=config.db_db,
            # user=config.db_user,
            # passwd=config.db_passwd,
            # keep_alive=True
        # )
        # print db    

    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream, with authentication
        stream = tweepy.streaming.Stream(auth, listener)
        
        #10. choose to track certain topics
        #stream.filter(track=['#bigdata','big data'])    
        
        #or receive a sample of all tweets - that's a lot!
        #9. comment out the following.
        stream.sample()

        while True:
            try:
                if stream.running is False:
                    print 'Stream stopped!'
                    break
                time.sleep(1) #sleep for 1 sec
            except KeyboardInterrupt:
                break
                
        stream.disconnect()
        
        print 'Bye!'
    else:
        #offline behavior
        print "offline mode"
        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
    
    #6. call simplemysql's end method to disconnect database.
    if _writedb_:
        db.end()
        