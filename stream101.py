'''
    a basic twitter API that spit out user and tweet text on screen.
    
    uncomment #1 to #3
    
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, raw_tweet):
        #process and raw_tweet here. 
        #1. load raw_tweet into an object called tweet using json.loads This process, also called deserialization, converts string into a navigatible json object.  
        # tweet = json.loads(raw_tweet)
        
        #2. using try ... except to print tweets in the form of created_at, username, tweet_text, having one extra line after the tweet.
        
        # try:
            # print "%s, %s\n%s\n\n"%(tweet['created_at'],tweet['user']['name'],tweet['text'])
        # except:
            # pass #ignore parsing errors
        
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':
    
    #create an oauth object
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.oauth_token, config.oauth_token_secret)

    #create a listener object
    listener = CustomStreamListener()

    #attach the listener object to twitter stream, with authentication
    stream = tweepy.streaming.Stream(auth, listener)
    
    #now start the listener process.
    
    #you may choose to track certain topics,
    #stream.filter(track=['#bigdata','big data'])    
    
    #3. or receive a sample of all tweets - that's a lot!
    #stream.sample()

    #once the stream started, it will be on a different process
    #so we can continue on to the next block of code
    #this while loop is designed as a monitor, so that it will check 
    #on the status of the listener once in a while.
    #it can also interrupt the listener on keyboard interruption (ctrl+c)
    
    while True:
        try:
            if stream.running is False:
                print 'Stream stopped!'
                break
            time.sleep(1) #sleep for 1 sec
        except KeyboardInterrupt:
            break
            
    stream.disconnect()
    print 'Bye!'
