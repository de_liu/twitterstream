'''
    in this example, we introduce some debugging facility
    including _debug_ and _online_ variable. This allows the listener to 
    feed on a locally stored tweet instead of a live connection. We also 
    save a twitter object in a local file for the purpose of debugging the listener.
    
    first do steps #1-5, run it. you expect to see an UnicodeEncodeError
    then do #6 to suppress the error
    then do #7 to run online mode, do #8 as needed.
    
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation

#1. import utils
# from pylib import utils 

#6. call enable_utf_print() from utils to enable printing of 
# -- utf characters (e.g. chinese /japanese).
# utils.enable_utf_print()

#2.add two global variables  
# _debug_ = True
# _online_ = False 

#7. Change the _online_ to true, then see what errors you get.
#   -- do step 8 as needed.

class CustomStreamListener(tweepy.StreamListener):
    def on_data(self, raw_tweet):
        #data comes in the raw json string
        # -- we need to convert that to a python object
        try:        
            tweet = json.loads(raw_tweet)
            #8. skip 'delete' messages, if it has a "delete" key. 
            # if tweet.get('delete'):
                # return True ## skip delete tweet            
            
            #print create_at, user name, and tweet text
            print "%s, %s\n%s\n\n"%(tweet['created_at'],tweet['user']['name'],tweet['text'])
        
        except Exception, e:
            #3. if _debug_ is True, dump tweet into /debug/tweet.txt using 
            # -- dump_json from utils and stop listener. 
            # -- otherwise ignore the exception.      
            
            # if _debug_:
                # #write the tweet to local disk for debugging
                # utils.dump_json(tweet,'debug/tweet.txt')
                # raise #stop the listener
            # else:
                # pass #ignore parsing errors            
            
                
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True  #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listen

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()        
    
    #4. if _online_ is True, do an online connection
    # -- uncomment the next line then indent everything till print "Bye!"
    
    # if _online_:  
    
    #create an auth object
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.oauth_token, config.oauth_token_secret)
        
    #attach the listener object to twitter stream, with authentication
    stream = tweepy.streaming.Stream(auth, listener)
    
    #you may choose to track certain topics
    #stream.filter(track=['#bigdata','big data'])    
    
    #or receive a sample of all tweets - that's a lot!
    stream.sample()

    while True:
        try:
            if stream.running is False:
                print 'Stream stopped!'
                break
            time.sleep(1) #sleep for 1 sec
        except KeyboardInterrupt:
            break
            
    stream.disconnect()
    print 'Bye!'
    
    #5. otherwise, print "offline mode"
    # -- call ondata method, and supply the debug/tweet_example_entities.txt 
    # -- as the raw tweet (use utils.read_file)
    # -- this way, we can debug the tweet parser.
    
    # else:
        # #offline mode
        # print "offline mode"
        # listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
    

        