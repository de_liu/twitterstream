'''
    in this example, we do some deeper parsing,
    extract useful information in a tweet into dict objects
    steps 
    #1-6, extract fields from tweet and put into tweet_data & user_data
    #7-9, check for missing tweet_id and user_id, and deal with special fields.
    #10 - 12, enrich data by adding capture_date and user_id
    #13, online debug    
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils

_debug_ = True #implement debuge model behavior
_online_ = False #read from online sources
#13. change _online_ to True to test online

utils.enable_utf_print()

#1. these fields can be directly copied to tweets_data
# tweet_fields = {
    # 'id':'tweet_id',
    # 'text':'tweet_text',
    # 'source':'source',
    # 'retweet_count':'retweet_count',    
    # 'in_reply_to_status_id':'reply_tweet_id',
    # 'in_reply_to_user_id':'reply_user_id',
    # 'favorite_count':'favorite_count',
    # 'created_at':'created_at',
# }

#2. these fields can be directly copied to user_data
# user_fields = {
    # 'name':'name',
    # 'screen_name':'screen_name',
    # 'id':'user_id',
# }

# 3. this function maps twitter fields into our dict keys.
# def get_fields(source, fields):
    # '''
        # given a source and a destination dict, copy the fields from source to destination according to the mapping provided by fields.  e.g. fields['fielda']='fieldb' would map fielda in source to fieldb in dest.     
    # '''
    # dest={}    
    # if not isinstance(source, dict):
        # print "source is not a dict" + type(source)

    # for field in fields:
        # if source.get(field):
            # dest[fields[field]]=source[field]
    # return dest    

class CustomStreamListener(tweepy.StreamListener):

    def on_data(self, raw_tweet):
        #data comes in the raw json string
        try:
            #10.  save time of event in capture_date 
            # -- using mysql_time() in utils
            #capture_date = utils.mysql_time()
            
            tweet = json.loads(raw_tweet)
            if tweet.get('delete'):
                return True ## skip delete tweet
            
            #4. prepare two dicts, one for each table    
            # tweet_data = {}
            # user_data = {}
            
            #5. retrieve and store data in tweet_data.
            # tweet_data.update(get_fields(tweet,tweet_fields))

            #7. Check if twitter has valid tweet_id
            # if not tweet_data.get('tweet_id'):
                # if _debug_:
                    # #save the tweet for debugging
                    # utils.dump_json(tweet,'debug/tweet.txt')
                # print('No tweet id')
                # return True  #discard this tweet            
            
            #8. created_at need to be converted to mysql_time before saving to tweet_data. note that created_at is nullable.
            # if tweet_data.get('created_at'):
                # tweet_data['created_at']=utils.utc_to_mysql_time(tweet_data['created_at'])
                
            #6. retrieve and store data in user_data.
            # if tweet.get('user'):
                # user_data.update(get_fields(tweet['user'],user_fields))
                
                #9. if user_id does not exist in user_data
                # -- we should deal with it like we deal with no tweet_id
                # if not user_data.get('user_id'):
                    # if _debug_:
                        # utils.dump_json(tweet,'debug/tweet%s.txt'%tweet_data.get('tweet_id'))
                    # print('No tweet user id')
                    # return True  #discard this tweet                 

            #enrichment 
            if tweet_data: 
                #11. add user_id & capture_date
                # tweet_data['user_id'] = user_data.get('user_id')
                # tweet_data['capture_date']=capture_date
                
                print tweet_data
                print
            if user_data:
                #12. add capture_date
                # -- add period, which is the date part of capture_date
                # -- we use this as part of the primary key for user
                # -- so that we can have at most one copy of a user per day. 
                
                # user_data['capture_date']=capture_date
                # user_data['period']=capture_date[:10]

                print user_data
                print            
        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_json(tweet,'debug/tweet.txt')
                raise 
            else:
                pass #ignore errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()

    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream, with authentication
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        #stream.filter(track=['#bigdata','big data'])    
        
        #or receive a sample of all tweets - that's a lot!
        stream.sample()

        while True:
            try:
                if stream.running is False:
                    print 'Stream stopped!'
                    break
                time.sleep(1) #sleep for 1 sec
            except KeyboardInterrupt:
                break
                
        stream.disconnect()
        print 'Bye!'
    else:
        #offline behavior
        #call listenr.on_data to test the parser.
        listener.on_data(utils.read_file('debug/tweet_example_entities.txt')) 
